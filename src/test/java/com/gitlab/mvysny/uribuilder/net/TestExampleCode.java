package com.gitlab.mvysny.uribuilder.net;

import java.net.URI;

public class TestExampleCode {
    /**
     * Just make sure that the example code from README.md compiles.
     */
    public static void main(String[] args) {
        URI uri = new URIBuilder("http://yourapi.com/rest")
                .addParameter("count", "5")
                .addParameter("filter", "full text search")
                .build();
        System.out.println(uri);
    }
}
