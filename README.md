# Apache-URIBuilder

This project is designed to complement the JDK11+ built-in [HttpClient](https://docs.oracle.com/en/java/javase/11/docs/api/java.net.http/java/net/http/HttpClient.html)
and provide the missing way of building URIs, via the `URIBuilder` class.

A fork of the [Apache HTTPComponents Core5](https://github.com/apache/httpcomponents-core), only aiming to provide
the URIBuilder functionality with no other functionality nor dependencies.

Forked off the httpcomponents-core 5.2.1. This library is only 33kb long, compared to `httpcore5` which is 850kb long.
This library has no dependencies other than the JDK 11+ itself.

Compatible with JDK 11 and higher.

## Examples

```java
URI uri = new URIBuilder("http://yourapi.com/rest")
        .addParameter("count", "5")
        .addParameter("filter", "full text search")
        .build();
System.out.println(uri);
```

prints `http://yourapi.com/rest?count=5&filter=full%20text%20search`

## Using with your project

The library is available in Maven Central, simply add this to your `build.gradle.kts`:
```kotlin
dependencies {
    implementation("com.gitlab.mvysny.apache-uribuilder:uribuilder:5.2.1")
}
```
or Maven `pom.xml`:
```xml
<project>
    <dependencies>
        <dependency>
            <groupId>com.gitlab.mvysny.apache-uribuilder</groupId>
            <artifactId>uribuilder</artifactId>
            <version>5.2.1</version>
        </dependency>
    </dependencies>
</project>
```

> Important: if you're already using the `org.apache.httpcomponents.core5:httpcore5` library version 5.2.1 or higher,
> you can use the `URIBuilder` class packaged in the `httpcore5` library directly instead.

## Kotlin API

You can use the following Kotlin utility methods:

```kotlin
/**
 * Parses this string as a `http://` or `https://` URL. You can configure the URL
 * (e.g. add further query parameters) in [block].
 * @throws IllegalArgumentException if the URL is unparsable
 */
fun String.buildUrl(block: URIBuilder.()->Unit = {}): URI {
    val uri = URIBuilder(this).apply(block).build()
    require(uri.scheme == "http" || uri.scheme == "https") { "Expected URL scheme 'http' or 'https' but got ${uri.scheme}: $uri" }
    return uri
}

/**
 * Builds a new [HttpRequest] using given URL. You can optionally configure the request in [block].
 * By default, the `GET` request gets built.
 */
fun URI.buildRequest(block: HttpRequest.Builder.()->Unit = {}): HttpRequest = HttpRequest.newBuilder(this).apply(block).build()

fun HttpRequest.Builder.post(body: String, mediaType: MediaType) {
    POST(BodyPublishers.ofString(body))
    header("Content-type", mediaType.toString())
}

fun HttpRequest.Builder.patch(body: String, mediaType: MediaType) {
    method("PATCH", BodyPublishers.ofString(body))
    header("Content-type", mediaType.toString())
}

fun HttpRequest.Builder.basicAuth(username: String, password: String) {
    val valueToEncode = "$username:$password"
    val h = "Basic " + Base64.getEncoder().encodeToString(valueToEncode.toByteArray())
    header("Authorization", h)
}

public data class MediaType(
  /**
   * Returns the high-level media type, such as "text", "image", "audio", "video", or "application".
   */
  val type: String,

  /**
   * Returns a specific media subtype, such as "plain" or "png", "mpeg", "mp4" or "xml".
   */
  val subtype: String,

  /**
   * Parameter names with their values, like `["charset" to "utf-8"]`.
   */
  private val parameterNamesAndValues: List<Pair<String, String>> = listOf()
) : Serializable {
  public fun charset(charset: String): MediaType = copy(parameterNamesAndValues = parameterNamesAndValues + listOf("charset" to charset))
  public fun charsetUtf8(): MediaType = charset("utf-8")

  override fun toString(): String = buildString {
    append(type)
    append('/')
    append(subtype)
    if (parameterNamesAndValues.isNotEmpty()) {
      append("; ")
      parameterNamesAndValues.forEach { append(it.first).append('=').append(it.second) }
    }
  }

  public companion object {
    public val json: MediaType = MediaType("application", "json")
    public val jsonUtf8: MediaType = json.charsetUtf8()
    public val xml: MediaType = MediaType("application", "xml")
    public val html: MediaType = MediaType("text", "html")
  }
}
```

Now, you can simply build the request via:

```kotlin
val request = "http://localhost:8080/person".buildUrl().buildRequest()

val url: HttpUrl = "http://localhost:8080/person".buildUrl {
  if (range != 0..Long.MAX_VALUE) {
    addParameter("offset", range.first.toString())
    addParameter("limit", range.length.toString())
  }
}
```
