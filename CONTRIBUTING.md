# Contributing

Thank you so much for making the library better.

The best way to report bugs and feature requests is to have them implemented upstream.
See [Get Involved with HttpComponents](https://hc.apache.org/get-involved.html) for more
info on how to open issues. After the issue has been implemented and released in a newer version
of `httpcore5`, please open a ticket in this repo and I'll update the sources and release a new updated version.

## Tests

Run `./gradlew test` to run all tests.

# Releasing

To release the library to Maven Central:

1. Edit `build.gradle.kts` and remove `-SNAPSHOT` in the `version=` stanza
2. Commit with the commit message of simply being the version being released, e.g. "1.2.13"
3. git tag the commit with the same tag name as the commit message above, e.g. `1.2.13`
4. `git push`, `git push --tags`
5. Run `./gradlew clean build publish`
6. Continue to the [OSSRH Nexus](https://oss.sonatype.org/#stagingRepositories) and follow the [release procedure](https://central.sonatype.org/pages/releasing-the-deployment.html).
7. Add the `-SNAPSHOT` back to the `version=` while increasing the version to something which will be released in the future,
   e.g. 1.2.14-SNAPSHOT, then commit with the commit message "1.2.14-SNAPSHOT" and push.
